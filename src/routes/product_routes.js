const router = require('express').Router();
const {protects} = require('../middlewares/auth');
const { postProduct, getPosts } = require('../controllers/product_controllers');

router.post('/', protects, postProduct);
router.get('/', getPosts);

module.exports = router;