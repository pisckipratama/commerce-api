const router = require('express').Router();
const {protects} = require('../middlewares/auth');
const { registerUser, loginUser, logout, getMe } = require('../controllers/auth_controllers');

router.post('/register', registerUser);
router.post('/login', loginUser);
router.post('/logout', logout);
router.get('/me', protects, getMe);

module.exports = router;