const path = require('path');
const Product = require('../models/product_models');
const ErrorHandler = require('../helpers/errorResponse');

// post product
exports.postProduct = async (req, res, next) => {
  const { title, brand, price, size, stock, description } = req.body;
  const payload = {
    title,
    brand,
    price,
    size,
    stock,
    description,
    user: req.user._id
  };

  try {
    if (req.user.role === 'customer') {
      return next(new ErrorHandler(`This user can not post product`, 400));
    }

    // if (!req.files) {
    //   return next(new ErrorHandler(`Please upload an image file`, 400));
    // }

    if (req.files) {
      const file = req.files.file;
  
      // make sure the image photo
      if (!file.mimetype.startsWith('image')) {
        return next(new ErrorHandler(`Please upload an image file`, 400));
      }
    
      // check file size
      if (file.size > process.env.MAX_FILE_UPLOAD) {
        return next(new ErrorHandler(`Please upload an image less than 2 MB`, 400));
      }
    
      // create custom filename
      file.name = `photo_${payload.user._id}_${Date.now()}${path.parse(file.name).ext}`;
      payload.image = file.name;
  
      file.mv(`${process.env.FILE_UPLOAD_PATH}/${file.name}`, async err => {
        if (err) {
          console.error(err);
          return next(new ErrorHandler(`Problem with file upload`, 500));
        }
    
        await Product.create(payload);
      });
    } else {
      payload.image = 'no-photo.jpg';
      await Product.create(payload);
    };

    res.status(201).json({ success: true, message: "create product success", content: payload });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: error.message });
  };
};

exports.getPosts = async (_, res, next) => {
  try {
    const product = await Product.find({});
    res.status(200).json({ success: true, content: product });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: error.message });
  }
}