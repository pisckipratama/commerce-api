const mongoose = require('mongoose');

const ProductSchema = new mongoose.Schema({
  title: {
    type: String,
    required: [true, 'title is not empty'],
  },
  brand: {
    type: String,
    required: [true, 'brand is not empty'],
  },
  price: {
    type: Number,
    required: [true, 'price is not empty'],
  },
  size: Array,
  description: String,
  image: {
    type: String,
    default: 'no-photo.jpg'
  },
  stock: Number,
  user: {
    type: mongoose.Schema.ObjectId,
    ref: 'User',
    required: true
  },
  createdat: {
    type: Date,
    default: Date.now
  }
});

module.exports = mongoose.model("Product", ProductSchema);