const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const connectDB = require('./src/configs/db');
const dotenv = require('dotenv');
const cors = require('cors');
const color = require('colors');
const errorHandler = require('./src/middlewares/error');
const fileUpload = require('express-fileupload');

dotenv.config();
connectDB();

const indexRouter = require('./src/routes/index');
const authRouter = require('./src/routes/auth_routes');
const productRouter = require('./src/routes/product_routes');

const app = express();

app.use(cors());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(fileUpload());
app.use(express.static(path.join(__dirname, 'src/public')));

const baseURL = '/api/v1';
app.use('/', indexRouter);
app.use(`${baseURL}/products`, productRouter);
app.use(`${baseURL}/auth`, authRouter);

app.use(errorHandler);

module.exports = app;
